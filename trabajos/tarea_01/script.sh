#!/bin /bash
#Este script muestra lo que aprendimos en la clase 2 y es el TP1

#Te da la bienvenida imprimiendo una leyenda con el comando echo
echo "HOLA!!" 
#imprime dos espacios
echo "" 
echo ""
#Imprime en pantalla lo que va haciendo cada comando y lo ejecuta
echo "Muestra de comandos"
echo "Muestra la fecha y hora actual con el comando DATE" 
date 
echo "Muestra la fecha actual con el formato que usamos en Argentina con DATE y sus modificadores"
date +%d/%m/%Y
#imprime un espacio
echo "" 
echo "Crea un directorio llamado PEPE con el comando MKDIR" 
mkdir -v PEPE
#Imprime un espacio
echo ""
echo "Muestra todos los directorios de donde estas parado con el comando LS -L"
ls -l
#Imprime un espacio
echo ""
echo "Muestra el TTY con el comando $ TTY"
$(tty)
echo "Muestra el usuario actual con el comando $ USER"
echo $USER
echo "Muestra la ruta del directorio actual con el comando $ HOME"
echo $HOME
echo ""
echo "hace un ping a un sitio 4 veces con comando PING y el modificador - C"
ping www.ole.com.ar -c 4
echo ""
echo "fin del programa"

